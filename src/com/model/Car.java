package com.model;

import java.time.LocalDate;

public class Car {
	private Brand brand;
	private double price;
	private String name;
	private Color color;
	private LocalDate createDate;
	
	public Car() {};
	
	public Car(Brand brand, double price, String name, Color color, LocalDate createDate) {
		super();
		this.brand = brand;
		this.price = price;
		this.name = name;
		this.color = color;
		this.createDate = createDate;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Brand getBrand() {
		return brand;
	}
	public void setBrand(Brand brand) {
		this.brand = brand;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public LocalDate getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDate createDate) {
		this.createDate = createDate;
	}
}
