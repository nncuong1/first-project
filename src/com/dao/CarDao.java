package com.dao;

import java.util.List;

import com.model.Car;

public interface CarDao {
	public void addCar(Car car);

	public void updateCar(Car product);

	public void deleteCar(int id);

	public Car findOne(int id);

	List<Car> findAll();
}
