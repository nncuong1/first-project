package com.jdk11;

import java.io.*;
import java.util.function.Function;

public class TestApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Function<String, Void> print = (var s) -> {
		    System.out.println(s);
		    return null;
		};
//		print.apply("Hello World !");
		 writeData();
		//readData();
	}

	private static void readData() {
		try {
			// Creating stream to read the object
			ObjectInputStream in = new ObjectInputStream(new FileInputStream("f.txt"));
			Student s = (Student) in.readObject();
			// printing the data of the serialized object
			System.out.println(s.id + " " + s.name);
			// closing the stream
			in.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private static void writeData() {
		try {
			Student s1 = new Student(212, "robert");
//			FileOutputStream fout = new FileOutputStream("f.txt");
//			ObjectOutputStream out = new ObjectOutputStream(fout);
//			out.writeObject(s1);
//			out.flush();
//			out.close();

			File file = new File("f.txt");
			//FileOutputStream fout = new FileOutputStream("f.txt");
			PrintWriter out = new PrintWriter(new FileWriter(file, true));
			String data = s1.id + "|" + s1.name;
	//		byte b[]=data.getBytes();
		//	fout.write(b);
		//	fout.close();
			out.println(data);

			out.close();

			System.out.println("success");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
